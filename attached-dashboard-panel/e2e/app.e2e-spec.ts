import { AttachedDashboardPanelPage } from './app.po';

describe('attached-dashboard-panel App', () => {
  let page: AttachedDashboardPanelPage;

  beforeEach(() => {
    page = new AttachedDashboardPanelPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
