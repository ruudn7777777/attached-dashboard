import { browser, element, by } from 'protractor';

export class AttachedDashboardPanelPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
