import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRouterModule } from './routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    AppRouterModule,
    BrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})


export class AppModule { }
