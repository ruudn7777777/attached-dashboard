import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppDashboardComponent } from './dashboard.component';
import { DashboardButtonSectionComponent } from './button-section/dashboard-button-section.component';


const appRouter: Routes = [
  {
    path: '',
    component: AppDashboardComponent
  }
];

@NgModule({
  declarations: [
    AppDashboardComponent,
    DashboardButtonSectionComponent
  ],
  imports: [
    RouterModule.forChild(appRouter)
  ],
  providers: [],
})


export class DashboardModule { }
